<?php


namespace App\DTO;


use App\Entity\Category;

class CategoryDTO
{
    private $id;
    /**
     * @var string
     */
    private $name;


    public function __construct(Category $c)
    {
        $this->id = $c->getId();
        $this->name = $c->getName();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return CategoryDTO
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return CategoryDTO
     */
    public function setName(string $name): CategoryDTO
    {
        $this->name = $name;
        return $this;
    }



}