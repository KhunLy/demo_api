<?php


namespace App\DTO;


use App\Entity\Category;
use App\Entity\Product;
use App\Entity\ProductCategory;

class ProductDetailsDTO
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $price;

    /**
     * @var \DateTime
     */
    private $expDate;

    /**
     * @var CategoryDTO[]
     */
    private $categories;


    public function __construct(Product $p, $categories)
    {
        $this->id = $p->getId();
        $this->name = $p->getName();
        $this->price = $p->getPrice();
        $this->expDate = $p->getExpDate();
        $this->categories = array_map(function($c){
            /** @var $c ProductCategory */
            return new CategoryDTO($c->getCategory());
        }, $categories);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ProductListDTO
     */
    public function setId(int $id): ProductDetailsDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ProductListDTO
     */
    public function setName(string $name): ProductDetailsDTO
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return ProductListDTO
     */
    public function setPrice(float $price): ProductDetailsDTO
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpDate(): \DateTime
    {
        return $this->expDate;
    }

    /**
     * @param \DateTime $expDate
     * @return ProductDetailsDTO
     */
    public function setExpDate(\DateTime $expDate): ProductDetailsDTO
    {
        $this->expDate = $expDate;
        return $this;
    }

    /**
     * @return CategoryDTO[]
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @param CategoryDTO[] $categories
     * @return ProductDetailsDTO
     */
    public function setCategories(array $categories): ProductDetailsDTO
    {
        $this->categories = $categories;
        return $this;
    }





}