<?php


namespace App\DTO;


use App\Entity\Product;

class ProductListDTO
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $price;


    public function __construct(Product $p)
    {
        $this->id = $p->getId();
        $this->name = $p->getName();
        $this->price = $p->getPrice();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ProductListDTO
     */
    public function setId(int $id): ProductListDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ProductListDTO
     */
    public function setName(string $name): ProductListDTO
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return ProductListDTO
     */
    public function setPrice(float $price): ProductListDTO
    {
        $this->price = $price;
        return $this;
    }



}