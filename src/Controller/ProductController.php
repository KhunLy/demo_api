<?php


namespace App\Controller;


use App\DTO\ProductDetailsDTO;
use App\DTO\ProductListDTO;
use App\Entity\Category;
use App\Entity\Product;
use App\Entity\ProductCategory;
use App\Form\ProductType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends AbstractFOSRestController
{
    /**
     * @Rest\Get(path="/api/product/{id}")
     * @Rest\View()
     */
    public function getById(Product $id)
    {
        $repo = $this->getDoctrine()->getRepository(ProductCategory::class);
        $liaisons = $repo->findBy(['product' => $id]);
        return new ProductDetailsDTO($id, $liaisons);
    }

    /**
     * @Rest\Get(path="/api/product")
     * @Rest\View()
     * @Rest\QueryParam(name="offset", requirements="\d+", default="0")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="100")
     * @Rest\QueryParam(name="keyword", default="")
     */
    public function getAll(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        $keyword = $paramFetcher->get('keyword');
        //$list = $this->getDoctrine()->getRepository(Product::class)->findAll();
        $list = $this->getDoctrine()->getRepository(Product::class)
            ->findWithLimit($offset, $limit, $keyword);
        return array_map(function($item) {
            return new ProductListDTO($item);
        }, $list);
    }

    /**
     * @Rest\Post(path="/api/product")
     * @Rest\View()
     */
    public function insert(Request $request)
    {
        $p = new Product();
        $form = $this->createForm(ProductType::class, $p, [
            "csrf_protection" => false
        ]);
        $json = $request->getContent();
        $data = json_decode($json, true);
        $form->submit($data);
        $form->handleRequest($request);
        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($p);
            $em->flush();
            return $p->getId();
        }
        return $form;
    }

    /**
     * @Rest\View()
     * @Rest\Put(path="/api/product/{id}")
     */
    public function update(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getRepository(Product::class);
        $product = $repo->find($id);
        $form = $this->createForm(ProductType::class, $product, [
            "csrf_protection" => false
        ]);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        $form->handleRequest($request);
        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $product->getId();
        }
        return $form;
    }

    /**
     * @Rest\View()
     * @Rest\Delete(path="/api/product/{id}")
     */
    public function delete(Request $request, Product $id)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($id);
        $em->flush();
        return true;
    }

    /**
     * @Rest\Post(path="/api/product/{productId}/add/{categoryId}")
     * @Rest\View()
     */
    public function addCategory(Product $productId, Category $categoryId)
    {
        $em = $this->getDoctrine()->getManager();
        $l = new ProductCategory();
        $l->setProduct($productId);
        $l->setCategory($categoryId);
        $em->persist($l);
        $em->flush();
        return true;
    }
}