<?php


namespace App\Controller;


use App\Entity\Product;
use App\Form\ProductType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;

class TestController extends AbstractFOSRestController
{
    /**
     * @Rest\Get(path="/api/test")
     * @Rest\View()
     */
    public function getAll()
    {
        return 0;
    }
}
