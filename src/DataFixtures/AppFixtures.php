<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager )
    {
        // $product = new Product();
        // $manager->persist($product);
        $user = new User();
        $user->setEmail("lykhun@gmail.com");
        $plainPassword = "toto";
        $hash = $this->encoder->encodePassword($user, $plainPassword);
        $user->setPassword($hash);
        $manager->persist($user);
        $manager->flush();
    }
}
