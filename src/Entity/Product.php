<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, nullable=false, unique=false)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var float
     * @ORM\Column(name="price", type="float", nullable=false, unique=false)
     * @Assert\NotBlank()
     */
    private $price;

    /**
     * @var ?\DateTime
     * @ORM\Column(name="expDate", type="date", nullable=true, unique=false)
     */
    private $expDate;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Product
     */
    public function setName(string $name): Product
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Product
     */
    public function setPrice(float $price): Product
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpDate()
    {
        return $this->expDate;
    }

    /**
     * @param mixed $expDate
     * @return Product
     */
    public function setExpDate($expDate)
    {
        $this->expDate = $expDate;
        return $this;
    }
}
